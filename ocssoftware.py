#!/usr/bin/env python
__author__ = "Anton Soroko"
__copyright__ = "Copyright (C) 2012 Anton Soroko"
__license__ = "GPL"
__version__ = "1.0"

import sys
import datetime
import MySQLdb
import smtplib
from email.mime.text import MIMEText
from _mysql_exceptions import ProgrammingError


def mysql_connect():
	db = MySQLdb.connect(host="db.test.com", user="ocs", passwd="ocs", db="ocsweb", charset='utf8')
	cursor = db.cursor()
	return (db, cursor)

def mysql_query(sql=""):
	global db
	global cursor
	#print sql
	cursor.execute(sql)
	data = cursor.fetchall()
	db.commit()
	#print data
	return data


db, cursor = mysql_connect()

msg=""
now = datetime.datetime.now()
subject="Software's Change Report for %s" % now.strftime("%Y-%m-%d %H:%M")

frommail="ocs@test.com"
tomail="admin@test.com"

sql = "select s.id FROM softwares s, hardware h where h.id=s.hardware_id and h.osname like '%Windows%'"
result2 = mysql_query(sql)

sql = "select s.id FROM softsnap s, hardware h where h.id=s.hardware_id and h.osname like '%Windows%'"
result = mysql_query(sql)

new_ids = list(set(result2) - set(result))
print len(new_ids)
old_ids = list(set(result) - set(result2))
checked_ids = []
print len(old_ids)
i=0

for id in new_ids:
	#sql = "select h.id hardid, h.name computer, h.workgroup, h.userid, h.ipaddr, s.id softid, s.name soft, s.version from hardware h, softwares s where h.id=s.hardware_id and s.id={0}".format(id[0])
	#print id
	sql = "select name, version, hardware_id from softwares where id={0}".format(id[0])
	result = mysql_query(sql)
	name=result[0][0]
	version=result[0][1]
	hardware_id=str(result[0][2])
	sql="select id from softsnap where name="+"'"+name+"'"+" and version="+"'"+version+"'"+" and hardware_id="+hardware_id
	result = mysql_query(sql)
	results_num = len(result)
	if results_num > 0:
		for i in range(results_num):
			checked_ids.append((result[i][0],))
	#if results_num == 1:
	#	checked_ids.append((result[0][0],))
	#elif results_num == 2:
	#	checked_ids.append((result[0][0],))
	#	checked_ids.append((result[1][0],))
	else:
	#if len(result) == 0:
		sql="select name, userid, ipaddr from hardware where id="+hardware_id
		result = mysql_query(sql)
		compname=result[0][0]
		username=result[0][1]
		ipaddr=result[0][2]
		msg+="The Software: %(name)s version: %(version)s was installed/changed on the computer: %(compname)s, IP: %(ipaddr)s, Last Login: %(username)s\n" % vars()

old_ids = list(set(old_ids) - set(checked_ids))
print len(old_ids)

for id in old_ids:
	#sql = "select h.id hardid, h.name computer, h.workgroup, h.userid, h.ipaddr, s.id softid, s.name soft, s.version from hardware h, softwares s where h.id=s.hardware_id and s.id={0}".format(id[0])
	sql = "select name, version, hardware_id from softsnap where id={0}".format(id[0])
	result = mysql_query(sql)
	name=result[0][0]
	version=result[0][1]
	hardware_id=str(result[0][2])
	#sadly, we still have to do check, because OCS mixes up i386 and amd64 packages. not anymore!
	#sql="select id, name, version from softwares where name="+'"'+name+'"'+" and version="+'"'+version+'"'+" and hardware_id="+hardware_id
	#we dont want to dublicate information about updated packages (same name, but different version)
	try:
		sql="select id, name, version from softwares where name="+"'"+name+"'"+" and hardware_id="+hardware_id
		result = mysql_query(sql)
	except ProgrammingError as e:
		print "Can't execute %s" % sql
		print e.message
		sys.exit(1)
	if len(result) == 0:
		sql="select name, userid, ipaddr from hardware where id="+hardware_id
		result = mysql_query(sql)
		compname=result[0][0]
		username=result[0][1]
		ipaddr=result[0][2]
		msg+="The Software: %(name)s version: %(version)s was uninstalled from the computer: %(compname)s, IP: %(ipaddr)s, Last Login: %(username)s\n" % vars()

print msg
sys.exit(0)

if msg !="":
	message = MIMEText(msg,_charset="utf-8")
	message['Subject'] = subject
	message['From'] = frommail
	message['To'] = tomail
	s = smtplib.SMTP('localhost')
	s.sendmail(frommail, tomail, message.as_string())
	s.quit()

sql="DROP TABLE softsnap"
result = mysql_query(sql)
sql="CREATE TABLE softsnap LIKE softwares"
result = mysql_query(sql)
sql="INSERT INTO softsnap SELECT * FROM softwares"
result = mysql_query(sql)

cursor.close()
db.close()
sys.exit(0)
