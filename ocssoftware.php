<?php
# This program will take a snapshot of the current software table in
# ocsweb and compare it to the day old snapshot.  Queries will then be
# run to identify and report any sofyware changes (via email). Once
# this is complete the old snapshot will be dropped and overwritten 
# with the current snapshot.
# Copyright Mike Seigafuse (mike@seigafuse.net) and Dioni Vidal (dionividal@gmail.com) 
# Released under the GPL license
# 
#
# Get the configuration file
require_once("ocsdiff_conf.php");
#include_once("ocsdiff_conf.php");
#$headers = "From: mail@email.com";
#$headers .= "Reply-To: mail@email.com";
ini_set('sendmail_from', 'ocs@test.com');
# Connect to database
$link = mysql_connect("$dbhost", "$dbuser", "$dbpass")
    or die("Could not connect : " . mysql_error());
mysql_select_db("$dbname") or die("Could not select database");
# Check to see if the softwares snapshot tables (softsnap) exist 
$softmatch = "FALSE";
$query = "show tables";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());
while ($line = mysql_fetch_assoc($results)) {
    foreach ($line as $col_value) {
	if ($col_value == "softsnap") {
	    $softmatch = "TRUE";
	    }
    }
}
if ($softmatch == "FALSE") {
    $query = "create table softsnap select * from softwares";
    $results = mysql_query($query)  or die("Query failed : " . mysql_error());
    die("The snapshot did not exist and no operations can be performed, the snapshot has been created for you so that future runs may operate\n");    
}
 
# Now loop through the defined domains (aka workgroups) and compare
# softwares status from the snapshots against the current data

for($i = 0; $i < sizeof($domains); $i++) {
	$entry = $domains[$i];
	$name = $entry['name'];
	$email = $entry['email'];
	$subject = "Software's Change Report for domain '$name'";
	$msg = "";
	# Now we have the domain name and contact, next get a current
	# list of softwares from this domain
	$query = "select h.id hardid,h.name computer, h.workgroup, h.userid, h.ipaddr, s.id softid, s.name soft, s.version from hardware h, softwares s where h.id=s.hardware_id and workgroup='$name' and h.osname like '%Windows%'";
	$softwaresresults = mysql_query($query)  or die("Query failed : " . mysql_error());
	while ($line = mysql_fetch_assoc($softwaresresults)) {
		$hardid = $line ['hardid'];
		$computername = $line['computer'];
		$ipaddr = $line['ipaddr'];
		$userid = $line['userid'];
		$currentsoftname = $line['soft'];
		$currentsoftversion = $line['version'];
		# Softwares checking section
		$query = "select name, version from softsnap where name=".'"'.$currentsoftname.'"'." and version=".'"'.$currentsoftversion.'"'." and hardware_id='$hardid'";
		$snapsoftresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$snapline = mysql_fetch_assoc($snapsoftresults);
		$snapsoftrows = mysql_num_rows($snapsoftresults);
		$snapsoftname = $snapline['name'];
    		$snapsoftversion = $snapline['version'];
		
    		# All information is now collected, now do the compare
		    
		if ( ($snapsoftrows == 0)  ){
		   $msg .= "O Software: $currentsoftname  Versao: $currentsoftversion, foi instalado/alterado no computador: $computername, IP: $ipaddr, Ultimo login: $userid \n";
		} # end of snaprows checking
		    
		    
		} # end of softwares in a domain loop -- while 

	# ------ checking deleted softwares ---------------------------------------
	#list softwares in softsnap to get deleted softwares ...
	
	$query = "select h.id hardid,h.name computer, h.workgroup, h.userid, h.ipaddr, s.id softid, s.name soft, s.version from hardware h, softsnap s where h.id=s.hardware_id and workgroup='$name' and h.osname like '%Windows%'";
	$delsnapsoftresults = mysql_query($query)  or die("Query failed : " . mysql_error());
	while ($snapline = mysql_fetch_assoc($delsnapsoftresults)) {
		$hardid = $snapline ['hardid'];
		$computername = $snapline['computer'];
		$ipaddr = $snapline['ipaddr'];
		$userid = $snapline['userid'];
		$deletedsoftname = $snapline['soft'];
		$deletedsoftversion = $snapline['version'];
		# Softwares checking section
		$query = "select name, version from softwares where name=".'"'.$deletedsoftname.'"'." and version=".'"'.$deletedsoftversion.'"'." and hardware_id='$hardid'";
		$softwaresresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$line = mysql_fetch_assoc($softwaresresults);
		$softwaresrows = mysql_num_rows($softwaresresults);
		
    		# All information is now collected, now do the compare
		    
		if ( ($softwaresrows == 0)  ){
		   $msg .= "O Software: $deletedsoftname  Versao: $deletedsoftversion, foi desinstalado do computador: $computername, IP: $ipaddr, Ultimo login: $userid \n";
		} # end of softwaresrows checking
		    
		    
		} # end of softwares in a domain loop -- second while 
	
	

	# send out the email and log for this domain        
	if( $msg != "" ){#print $msg; #DEBUG em linha de comando
                         #log	
	                 $file_log = "/var/log/ocsinventory-server/ocssoftware.log";
	                 $handle = fopen($file_log, 'a');
	                 if (!$handle) {
	                     $log_infoh = "N�o foi poss�vel abrir o arquivo ($file_log)";
	                     shell_exec("echo `date` '$log_infoh'  >> /var/log/ocsinventory-server/ocssoftware_erro.log");
		         }
		         if (fwrite($handle, $msg) === FALSE) {
		             $log_infow = "N�o foi poss�vel escrever no arquivo ($file_log)";
		             shell_exec("echo `date` '$log_infow' >> /var/log/ocsinventory-server/ocssoftware_erro.log");
		         }
		         fclose($handle);
		         
		         #mail
	                 mail($email, $subject, $msg);
	                 }
	
	} # end of domain loop
	
# Now that the report has been run, drop the softsnap table
# and take new snapshots
$query = "drop table softsnap";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());
$query = "create table softsnap select * from softwares";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());


mysql_close();

# The End :)
?>
