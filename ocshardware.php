<?php
# This program will take a snapshot of the current hardware table in
# ocsweb and compare it to the day old snapshot.  Queries will then be
# run to identify and report any hardware changes (via email). Once
# this is complete the old snapshot will be dropped and overwritten 
# with the current snapshot.
# Copyright Mike Seigafuse (mike@seigafuse.net) and Dioni Vidal (dionividal@gmail.com)
# Released under the GPL license
# 
#
# Get the configuration file
require_once("ocsdiff_conf.php");
#include_once("ocsdiff_conf.php");
#$headers = "From: mail@email.com";
#$headers .= "Reply-To: mail@email.com";
ini_set('sendmail_from', 'ocs@test.com');
# Connect to database
$link = mysql_connect("$dbhost", "$dbuser", "$dbpass")
    or die("Could not connect : " . mysql_error());
mysql_select_db("$dbname") or die("Could not select database");
# Check to see if the hardware and drives snapshot tables (hardsnap and 
# drivesnap) exist
$hardmatch = "FALSE";
$drivematch = "FALSE";
$videomatch = "FALSE";
$soundmatch = "FALSE";
$query = "show tables";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());
while ($line = mysql_fetch_assoc($results)) {
    foreach ($line as $col_value) {
	if ($col_value == "hardsnap") {
	    $hardmatch = "TRUE";
	    }
	if ($col_value == "drivesnap") {
	    $drivematch = "TRUE";
	    }
	if ($col_value == "videosnap") {
	    $videomatch = "TRUE";
	    }
	if ($col_value == "soundsnap") {
	    $soundmatch = "TRUE";
	    }
	               
    }
}
if ($hardmatch == "FALSE") {
    $query = "create table hardsnap select * from hardware";
    $results = mysql_query($query)  or die("Query failed : " . mysql_error());
}
if ($drivematch == "FALSE") {
    $query = "create table drivesnap select * from drives";
    $results = mysql_query($query)  or die("Query failed : " . mysql_error());
}
if ($videomatch == "FALSE") {
    $query = "create table videosnap select * from videos";
    $results = mysql_query($query)  or die("Query failed : " . mysql_error());
}
if ($soundmatch == "FALSE") {
    $query = "create table soundsnap select * from sounds";
    $results = mysql_query($query)  or die("Query failed : " . mysql_error());
}
if (($hardmatch == "FALSE") || ($drivematch == "FALSE") || ($videomatch == "FALSE") || ($soundmatch == "FALSE")) {
    die("The snapshot did not exist and no operations can be performed, the snapshot has been created for you so that future runs may operate\n");
} 
# Now loop through the defined domains (aka workgroups) and compare
# hardwarestatus from the snapshots against the current data

for($i = 0; $i < sizeof($domains); $i++) {
	$entry = $domains[$i];
	$name = $entry['name'];
	$email = $entry['email'];
	$subject = "Hardware's Change Report for domain $name";
	$msg = "";
	# Now we have the domain name and contact, next get a current
	# list of deviceIDs from this domain
	$query = "select id,deviceid, name, userid from hardware where workgroup='$name'";
	$deviceresults = mysql_query($query)  or die("Query failed : " . mysql_error());
	while ($line = mysql_fetch_assoc($deviceresults)) {
		$hardid = $line['id'];
		$deviceid = $line['deviceid'];
		$computername = $line['name'];
		$userid = $line['userid'];
		# Memory (RAM) checking section (just collect data for now)
		$query = "select memory from hardware where deviceid='$deviceid'";
		$curramresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$ramline = mysql_fetch_assoc($curramresults);
		$query = "select memory from hardsnap where deviceid='$deviceid'";
		$snapramresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$snapline = mysql_fetch_assoc($snapramresults);
		$snapramrows = mysql_num_rows($snapramresults);
		$currentram = $ramline['memory'];
    		$snapram = $snapline['memory'];
		# Disk checking section
		$query = "select letter from drives 
			where hardware_id='$hardid' and 
			type = 'Hard Drive'";
		$currdiskresults = mysql_query($query)  or die("Query failed : " . mysql_error());

		$query = "select letter from drivesnap
                        where hardware_id='$hardid' and 
                        type = 'Hard Drive'";
		$snapdiskresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$curdskcnt = mysql_num_rows($currdiskresults);
		$snapdskcnt = mysql_num_rows($snapdiskresults);
		
		#IPADDR check section
		$query = "select ipaddr from hardware where deviceid='$deviceid'";
		$curripaddrresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$ipaddrline = mysql_fetch_assoc($curripaddrresults);
		$query = "select ipaddr from hardsnap where deviceid='$deviceid'";
		$snapipaddrresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$snapline = mysql_fetch_assoc($snapipaddrresults);
		$snapipaddrrows = mysql_num_rows($snapipaddrresults);
		$currentipaddr = $ipaddrline['ipaddr'];
    		$snapipaddr = $snapline['ipaddr'];
    		
    		#Processor check section
    		$query = "select processort from hardware where deviceid='$deviceid'";
		$currprocessorresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$processorline = mysql_fetch_assoc($currprocessorresults);
		$query = "select processort from hardsnap where deviceid='$deviceid'";
		$snapprocessorresults = mysql_query($query)  or die("Query failed : " . mysql_error());
		$snapline = mysql_fetch_assoc($snapprocessorresults);
		$snapprocessorrows = mysql_num_rows($snapprocessorresults);
		$currentprocessor = $processorline['processort'];
    		$snapprocessor = $snapline['processort'];
    		
    		#video, sound, and other cards have more than one cards installed, we have to use while loop 
    		
    		#video cards check section
    		$msgvideo = "";
    		$query = "select name,chipset from videos where hardware_id='$hardid'";
    		$currvideoresults = mysql_query($query)  or die("Query failed : " . mysql_error());
                while ($videoline = mysql_fetch_assoc($currvideoresults)) {
                    $currentvideoname = $videoline['name'];
                    $currentvideochipset = $videoline['chipset'];
                    $query = "select name,chipset from videosnap where hardware_id='$hardid' and name=".'"'.$currentvideoname.'"'." and chipset=".'"'.$currentvideochipset.'"';
                    $snapvideoresults = mysql_query($query)  or die("Query failed : " . mysql_error());
                    $snapline = mysql_fetch_assoc($snapvideoresults);
                    $snapvideorows = mysql_num_rows($snapvideoresults);
                    if ($snapvideorows == 0){
                       $msgvideo .= "The video card ".$currentvideoname." chipset ".$currentvideochipset." was installed on the computer ".$computername.", IP ".$currentipaddr.", last login: $userid\n";
                    }                
                } # end of while video install
                # checking removed video card
                $query = "select name,chipset from videosnap where hardware_id='$hardid'";
    		$removedsnapvideoresults = mysql_query($query)  or die("Query failed : " . mysql_error());
    		while ($snapline = mysql_fetch_assoc($removedsnapvideoresults)) {
    		    $removedvideoname = $snapline['name'];
    		    $removedvideochipset = $snapline['chipset'];
    		    $query = "select name,chipset from videos where hardware_id='$hardid' and name=".'"'.$removedvideoname.'"'." and chipset=".'"'.$removedvideochipset.'"';
    		    $videoresults = mysql_query($query)  or die("Query failed : " . mysql_error());
    		    $videoline = mysql_fetch_assoc($videoresults);
                    $videorows = mysql_num_rows($videoresults);
                    if ($videorows == 0){
                       $msgvideo .= "The video card ".$removedvideoname." chipset ".$removedvideochipset." was removed from the computer ".$computername.", IP ".$currentipaddr.", last login: $userid\n";
                    }    		
    		}# end of while video remove
    		
    		#sound cards check section
    		$msgsound = "";
    		$query = "select name from sounds where hardware_id='$hardid'";
    		$currsoundresults = mysql_query($query)  or die("Query failed : " . mysql_error());
    		while ($soundline = mysql_fetch_assoc($currsoundresults)) {
    		$currentsoundname = $soundline['name'];
    		$query = "select name from soundsnap where hardware_id='$hardid' and name=".'"'.$currentsoundname.'"';
    		$snapsoundresults = mysql_query($query)  or die("Query failed : " . mysql_error());
    		$snapline = mysql_fetch_assoc($snapsoundresults);
    		$snapsoundrows = mysql_num_rows($snapsoundresults);
    		if ($snapsoundrows == 0){
    		   $msgsound .= "The soundcard ".$currentsoundname." was installed on the computer ".$computername.", IP ".$currentipaddr.", last login: $userid\n";
    		}    		
    		} # end of while sound install
    		#checking removed sound card
    		$query = "select name from soundsnap where hardware_id='$hardid'";
    		$removedsnapsoundresults = mysql_query($query)  or die("Query failed : " . mysql_error());
    		while ($snapline = mysql_fetch_assoc($removedsnapsoundresults)) {
    		    $removedsoundname = $snapline['name'];
    		    $query = "select name from sounds where hardware_id='$hardid' and name=".'"'.$removedsoundname.'"';
    		    $soundresults = mysql_query($query)  or die("Query failed : " . mysql_error());
    		    $soundline = mysql_fetch_assoc($soundresults);
                    $soundrows = mysql_num_rows($soundresults);
                    if ($soundrows == 0){
                       $msgsound .= "The soundcard ".$removedsoundname." was removed from the computer ".$computername.", IP ".$currentipaddr.", last login: $userid\n";
                    }    		
    		}# end of while sound remove
    		
    		
    		# All information is now collected, now do the compare
		if (($snapramrows == 0) && ($snapdskcnt == 0) && ($snapipaddrrows == 0)) {
			$msg .= "The computer $computername was added in inventory with $currentram MB of RAM memory, with the following ip address: $currentipaddr and $curdskcnt hdd, last login: $userid\n";
			} else {
		if (!($currentram == $snapram)) {
        		$msg .= "The computer $computername had changed the RAM memory from $snapram MB to $currentram MB, last login: $userid\n";
        		} # end of RAM check portion of else loop
		if (!($curdskcnt == $snapdskcnt)) {
			$msg .= "The computer $computername had $snapdskcnt HDD and now has $curdskcnt HDD, last login: $userid\n";
			} # end of disk check portion of else loop
		if (!($currentipaddr == $snapipaddr)) {
			$msg .= "The computer ".$computername." had changed the IP address from ".$snapipaddr." to ".$currentipaddr.", last login: $userid\n";
			} # end of ipaddr check portion of else loop
		if (!($currentprocessor == $snapprocessor)) {
			$msg .= "The computer ".$computername." had changed the processor from ".$snapprocessor." to ".$currentprocessor.", last login: $userid\n";
			} # end of processor check portion of else loop	
                if ($msgvideo != "") {
			$msg .= $msgvideo;
			} # end of video check portion of else loop
		if ($msgsound != "") {
			$msg .= $msgsound;
			} # end of sound check portion of else loop
				
		    } # end of row check loop - else
		    
		} # end of devices in a domain loop - while

	# send out the email for this domain
	if( $msg != "" ){#print $msg; #DEBUG em linha de comando
                         #log
                         $file_log = "/var/log/ocsinventory-server/ocshardware.log";
                         $handle = fopen($file_log, 'a');
                         if (!$handle) {
                            $log_infoh = "N�o foi poss�vel abrir o arquivo ($file_log)";
                            shell_exec("echo `date` '$log_infoh'  >> /var/log/ocsinventory-server/ocshardware_erro.log");
                         }
                         if (fwrite($handle, $msg) === FALSE) {
                            $log_infow = "N�o foi poss�vel escrever no arquivo ($file_log)";
                            shell_exec("echo `date` '$log_infow' >> /var/log/ocsinventory-server/ocshardware_erro.log");
                         }
                         fclose($handle);
	
                         #mail
	                 mail($email, $subject, $msg);
	                 }
	
	} # end of domain loop - for

# Now that the report has been run, drop the hardsnap and drivesnap tables
# and take new snapshots
$query = "drop table hardsnap";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());
$query = "create table hardsnap select * from hardware";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());

$query = "drop table drivesnap";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());
$query = "create table drivesnap select * from drives";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());

$query = "drop table videosnap";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());
$query = "create table videosnap select * from videos";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());

$query = "drop table soundsnap";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());
$query = "create table soundsnap select * from sounds";
$results = mysql_query($query)  or die("Query failed : " . mysql_error());

mysql_close();

# The End :)
?>
