<?php
# ocsdiff configuration file
#
# Database host
$dbhost = "db.test.com";
$dbname = "ocsweb";
$dbuser = "ocs";
$dbpass = "ocs";
$adminemail = "admin@test.com";
$diskthreshold = 8; # percentage free space threshold
$domains = array(
		0 => array(
			'name' => 'test.com',
			'email' => "admin@test.com"),
		1 => array(
			'name' => 'WORKGROUP',
			'email' => "admin@test.com"));
$diskdomains = array(
		0 => array(
			'name' => 'test.com',
			'email' => "admin@test.com"),
		1 => array(
			'name' => 'WORKGROUP',
			'email' => "admin@test.com"));
?>
